/*
 *	FILE:	Player.cs
 *	AUTHOR:	mmiddleton
 *	DATE:	11 Oct. 2017
 * 
 * 	DESCRIPTION:
 * 	This script is to be used as object that holds Player information.
 */
 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    // Public Variables:
    public string Name;                             // Player's name
    public GameManagerScript.Faction Faction;       // Player's Faction
    public int Value;                               // Allows for quick switching of current-player-based operations
    public int Score;                               // Amount of Games won again current opponent
    public GameObject UnitStructureContainer;       // Empty containing  all of the player's units

    /*
        NOTETOSELF:
        These fields are serialized so I can view values in editor.
        Will remove after confirming they work.
    */

    // Private Variables - Manage these in-engine:
    [SerializeField]
    private bool _towerIsPlaced;
    [SerializeField]
    private bool _towerIsAlive;

    [SerializeField]
    private bool _trapIsPlaced;
    [SerializeField]
    private bool _trapIsAlive;

    [SerializeField]
    private bool _wallIsPlaced;
    [SerializeField]
    private bool _wallIsAlive;
    [SerializeField]
    private int _maxAttackUnits;
    [SerializeField]
    private int _maxDefenseUnits;
    [SerializeField]
    private int _maxSwarmUnits;

    


    // Private Variables:
    private int _numAttackUnitsPlaced;
    private int _numDefenseUnitsPlaced;
    private int _numSwarmUnitsPlaced;
    private int _numSwarmUnitsAllowedInitially;
    private bool _hasPlacedSwarmThisTurn;




    public void Initialize () {
        switch (Faction) {
            case GameManagerScript.Faction.ONE:
                _maxAttackUnits = 5;
                _maxDefenseUnits = 3;
                _numAttackUnitsPlaced = 0;
                _numDefenseUnitsPlaced = 0;
                break;

            case GameManagerScript.Faction.TWO:
                _maxAttackUnits = 3;
                _maxDefenseUnits = 5;
                _numAttackUnitsPlaced = 0;
                _numDefenseUnitsPlaced = 0;
                break;

            case GameManagerScript.Faction.THREE:
                _maxAttackUnits = 4;
                _maxDefenseUnits = 4;
                _numAttackUnitsPlaced = 0;
                _numDefenseUnitsPlaced = 0;
                break;

            case GameManagerScript.Faction.FOUR:
                _maxSwarmUnits = 12;
                _numSwarmUnitsAllowedInitially = 8;
                _numSwarmUnitsPlaced = 0;
                break;
        }
    }



    public void CopyPlayerData (Player toBeCopied) {
        this.Name = toBeCopied.Name;
        this.Faction = toBeCopied.Faction;
        Initialize ();
    }

    /*
        Check if can place UnitStructure
    */
    private bool _PlaceTowerIfPossible () {
        if (!_towerIsPlaced) {
            _towerIsPlaced = _towerIsAlive = true;
            return true;
        }
        
        return false;
    }
    private bool _PlaceTrapIfPossible () {
         if (!_trapIsPlaced) {
            _trapIsPlaced = _trapIsAlive = true;
            return true;
        }

        return false;
    }
    private bool _PlaceWallIfPossible () {
         if (!_wallIsPlaced) {
            _wallIsPlaced = _wallIsAlive = true;
            return true;
        }

        return false;
    }

    private bool _PlaceAttackUnitIfPossible () {
        if (_numAttackUnitsPlaced < _maxAttackUnits) {
            _numAttackUnitsPlaced++;
            return true;
        }

        return false;
    }

    private bool _PlaceDefenseUnitIfPossible () {
        if (_numDefenseUnitsPlaced < _maxDefenseUnits) {
            _numDefenseUnitsPlaced++;
            return true;
        }

        return false;
    }

    private bool _PlaceSwarmUnitIfPossible () {
        if ((GameManagerScript.Instance.Phase < 3 && _numSwarmUnitsPlaced < _numSwarmUnitsAllowedInitially) || (GameManagerScript.Instance.Phase == 3 && _numSwarmUnitsPlaced < _maxSwarmUnits)) {
            _numSwarmUnitsPlaced++;
            return true;
        }
        
        return false;
    }

    public bool CanPlaceTower () {
        return _towerIsPlaced;
    }

    //  ///////////////////////////////////////////////////////



	// Checks the attempted unit placement against the 
	public bool CheckLegalPlacement (UnitStructure.UnitStructureType type, GameboardTileScript tile) {
        // We can only place units on our side!
        if (tile.PlayerSide == Value) {
            // If we're on a plateau, the unit in question MUST be a tower.
            if (tile.IsPlateau && type == UnitStructure.UnitStructureType.TOWER)
                return _PlaceTowerIfPossible();

            // Else, we MUST be on the ground to place a unit.
            else {
                if (!tile.IsPlateau && tile.IsNotOccupied())
                    switch (type) {
                        case UnitStructure.UnitStructureType.ATTACK:
                            return _PlaceAttackUnitIfPossible ();

                        case UnitStructure.UnitStructureType.DEFENSE:
                            return _PlaceDefenseUnitIfPossible ();

                        case UnitStructure.UnitStructureType.SWARM:
                            return _PlaceSwarmUnitIfPossible ();

                        case UnitStructure.UnitStructureType.TOWER:
                            return _PlaceTowerIfPossible ();

                        case UnitStructure.UnitStructureType.WALL:
                            return _PlaceWallIfPossible ();

                        case UnitStructure.UnitStructureType.TRAP:
                            return _PlaceTrapIfPossible ();

                        default:
                            Debug.Log ("ERROR [Player.CheckLegalPlacement] - Unsupported type (" + type.ToString () + ")");
                            return false;
                    }
            }
        }

        return false;
	}

    
    public int GetUnitCount () {
        return _numAttackUnitsPlaced + _numDefenseUnitsPlaced + _numSwarmUnitsPlaced;
    }

    public int GetUnitStructureCount () {
       int toBeReturned = _numAttackUnitsPlaced + _numDefenseUnitsPlaced + _numSwarmUnitsPlaced;
       toBeReturned += (_towerIsAlive) ? (1) : (0);
       toBeReturned += (_trapIsAlive) ? (1) : (0);
       toBeReturned += (_wallIsAlive) ? (1) : (0);
       return toBeReturned;
    }


    // Returns true if the following are true: Faction is SWARM, Hasn't placed a Unit this turn, Number placed is less than max allowed, and Enemy is not in Current Player's land.
    public bool CanPlaceSwarmUnit () {
        if (Faction == GameManagerScript.Faction.FOUR)
            if (!_hasPlacedSwarmThisTurn && _numSwarmUnitsPlaced < _maxSwarmUnits)
                if (GameManagerScript.Instance.Gameboard.CheckSwarmIsUninvaded ())
                    return true;

        return false;
    }


    
    // Sets _hasPlacedSwarmThisTurn to true.
    public void LockSwarmPlacement () {
        _hasPlacedSwarmThisTurn = true;
    }



    // Sets _hasPlacedSwarmThisTurn to false.
    public void ResetSwarmLock () {
        _hasPlacedSwarmThisTurn = false;
    }


    public void KillUnit (UnitStructure deceased, bool isTrap = false) {
        if (!isTrap)
            GameManagerScript.Instance.SoundManager.PlaySound ("Death");
       
        switch (deceased.Type) {
            case UnitStructure.UnitStructureType.ATTACK:
                _numAttackUnitsPlaced--;
                break;

            case UnitStructure.UnitStructureType.DEFENSE:
                _numDefenseUnitsPlaced--;
                break;

            case UnitStructure.UnitStructureType.SWARM:
                _numSwarmUnitsPlaced--;
                break;

            case UnitStructure.UnitStructureType.WALL:
                _wallIsAlive = false;
                break;

            case UnitStructure.UnitStructureType.TRAP:
                _trapIsAlive = false;
                break;

            case UnitStructure.UnitStructureType.TOWER:
                _towerIsAlive = false;
                break;
        }

        Destroy (deceased.gameObject);
    }

    public void ResetData (bool isFromMainMenu = false) {
        if (isFromMainMenu)
            Score = 0;
        _numAttackUnitsPlaced = _numDefenseUnitsPlaced = _numSwarmUnitsPlaced = 0;
        _towerIsPlaced = _towerIsAlive = _trapIsPlaced = _trapIsAlive = _wallIsPlaced = _wallIsAlive = false; 
    }
    public void FullResetData () {
        Score = 0;
        _numAttackUnitsPlaced = _numDefenseUnitsPlaced = _numSwarmUnitsPlaced = 0;
        _towerIsPlaced = _towerIsAlive = _trapIsPlaced = _trapIsAlive = _wallIsPlaced = _wallIsAlive = false; 
    }
}