﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class MainMenuScript : MenuScript {
	[SerializeField]
	private Button[] _buttons;
	private Animator[] _animators;

	
	private enum _Buttons {PLAY = 0, RESET = 1, EXIT = 2}
	[SerializeField]
	private int _focus;

	public override void Load () {
		_focus = (int)_Buttons.PLAY;
		_animators = new Animator[_buttons.Length];
		
		for (int c = 0; c < _buttons.Length; c++)
			_animators[c] = _buttons[c].GetComponent<Animator> ();
		
		_animators[_focus].SetBool ("HasFocus", true);
	}


	public override void SetCurrentFocus (MenuDirection direction) {
		bool skip = direction == MenuDirection.UP && (int)_focus == 0;
		skip = skip || (direction == MenuDirection.DOWN && (int)_focus == (_buttons.Length - 1));
		
		if (!skip) {
			foreach (Animator anim in _animators)
				anim.SetBool ("HasFocus", false);

			_focus += (int)direction;
		
			_animators[_focus].SetBool ("HasFocus", true);
		}
	}

	public override void SelectCurrentFocus () {
		switch (_focus) {
			case (int)_Buttons.PLAY:
				MainMenuManagerScript.Instance.ActiveMenu = MainMenuManagerScript.MenuType.GAME_SETUP;
				break;

			case (int)_Buttons.RESET:
				GameManagerScript.Instance.ResetData (false, true);
				break;

			case (int)_Buttons.EXIT:
				Application.Quit ();
				break;
		}
	}
}
