﻿/*
 *	FILE:	UIManagerScript.cs
 *	AUTHOR:	mmiddleton
 *	DATE:	16 SEP 2017
 * 
 * 	DESCRIPTION:
 * 	This script is to be connected to the MAIN MENU MANAGER object. It will hold the code for altering the Main Menu UI.
 * 
 * 	REFERENCED FROM:
 * 	- MainMenuManagerScript
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuManagerScript : MonoBehaviour {
	public enum MenuType {TITLE = 0, GAME_SETUP = 1, SETTINGS = 2}
	private static MainMenuManagerScript _instance = null;
	public static MainMenuManagerScript Instance {
		get {
			if (_instance == null) {
				GameObject newMainMenuManagerInstance = new GameObject ();
				newMainMenuManagerInstance.name = "Main Menu Manager";
				DontDestroyOnLoad (newMainMenuManagerInstance);
			}

			return _instance;
		}
	}




	public MenuType ActiveMenu {
		set  {
			_activeMenu.gameObject.SetActive (false);
			_SetActiveMenu (value);
		}
	}
	private MenuScript _activeMenu;


	[SerializeField]
	private MenuScript[] _menus;


	void Awake () {
		if (_instance == null) {
			_instance = this;
			_SetActiveMenu (MenuType.TITLE);
		}
	}


	public void SelectCurrentMenuItem () {
		GameManagerScript.Instance.SoundManager.PlaySound ("MenuSelect");
		_activeMenu.SelectCurrentFocus ();
	}


	public void SwitchFocusPlayer (int newTargetPlayer) {
		if (_activeMenu is GameSetupMenuScript)
			((GameSetupMenuScript)_activeMenu).ChangeFocusPlayer (newTargetPlayer);
	}

	public void BackMenu () {
		Debug.Log ("Back!");
		if (!(_activeMenu is MainMenuScript))
			ActiveMenu = (int)MenuType.TITLE;
	}

	public void MoveInMenu (MenuScript.MenuDirection direction) {
		GameManagerScript.Instance.SoundManager.PlaySound ("MenuNav");
		_activeMenu.SetCurrentFocus (direction);
	}
	
	public void StartGame () {
		// GameManagerScript.Instance.GameSetup ();
		SceneManager.LoadScene ("Gameboard");
	}




	private void _SetActiveMenu (MenuType designation) {
		_activeMenu = _menus[(int)designation];
		_activeMenu.Load ();
		_activeMenu.gameObject.SetActive (true);
	}
}
