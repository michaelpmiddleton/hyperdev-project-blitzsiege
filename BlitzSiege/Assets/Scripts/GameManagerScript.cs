﻿/*
 *	FILE:	GameManagerScript.cs
 *	AUTHOR:	mmiddleton
 *	DATE:	8 SEP 2017
 * 
 * 	DESCRIPTION:
 * 	This script is to be connected to the GAMEMANAGER Empty. It will link to the varying interfaces managing turns, player information, [more to be added?].
 * 
 */
 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManagerScript : MonoBehaviour {
	// Singleton Declaration:
	private static GameManagerScript _instance = null;
	public static GameManagerScript Instance {
		get {
			// Find the current GameManager.
			_instance = FindObjectOfType <GameManagerScript> ();

			if (_instance == null) {
				Debug.Log ("GameManagerInstance created!");
				GameObject newGMInstance = new GameObject ();
				newGMInstance.name = "GameManagerScript";
				newGMInstance.AddComponent<GameManagerScript> ();
				newGMInstance.AddComponent<InputManagerScript> ();
				DontDestroyOnLoad (newGMInstance);
			}

			return _instance;
		}
	}

	// Enum & Static Methods:
	public static void DebugOutput (string message) {
		if (DebugMode)
			Debug.Log (message);
	}

	public enum Faction {UNSELECTED = 0, ONE = 1, TWO = 2, THREE = 3, FOUR = 4};


	// Public Variables:
	public Player PlayerOne;
	public Player PlayerTwo;
	public Player CurrentPlayer;
	public UIManagerScript UIManager;
	public GameboardTileScript FocusTile;
	public GameboardScript Gameboard;
	public CombatManagerScript CombatManager;
	public InputManagerScript InputManager;
	public SoundManagerScript SoundManager;
	public GameObject HealthBarPrefab;

	public int Phase;
	public int Turn;
	public float TimerMax;
	public float TitleCardTimeoutMax;
	public bool BattleLog;
	public bool ConduitEntered;
	public bool GameOver { get { return _GameOver (); }}



	// Private Variables - Manage these in Engine:
	[SerializeField]
	private Player BASE_PLAYER_ONE;
	[SerializeField]
	private Player BASE_PLAYER_TWO;
	[SerializeField]
	private float _normalTurnLength;
	[SerializeField]
	private bool _launchedSceneDirectly;
	

	// Private Variables:
	private float _timerValue;
	private float _titleTimeout;
	private static bool DebugMode;
	private static bool DeveloperCommentary;
	private string _activeSceneName;
	[SerializeField]
	private Player _winner;
	


	/*
		NOTETOSELF:
		- Need to load in the UI manager when switching scenes!
		- " gameboard.
		- In the GameboardScene, curently using DebugPlayerOne & DebugPlayerTwo prefabs.	
	*/


	private void Awake () {
		if (!_instance) {
			Debug.Log ("Creating GameManagerScript.");
			_instance = this;
			_timerValue = TimerMax;
			_titleTimeout = TitleCardTimeoutMax;
			_winner = null;
			
			InputManager = transform.GetComponent<InputManagerScript> ();
			
			
			//		Various Output Switches: /////
			DebugMode = false;
			DeveloperCommentary = false;
			BattleLog = false;
			//	///////////////////////////////////


			PlayerOne = Instantiate (BASE_PLAYER_ONE);
			PlayerTwo = Instantiate (BASE_PLAYER_TWO);
			CurrentPlayer = PlayerOne;

			if (_launchedSceneDirectly) {
				PlayerOne.Initialize ();
				PlayerOne.UnitStructureContainer = GameObject.Find ("Player One Container");

				PlayerTwo.Initialize ();
				PlayerTwo.UnitStructureContainer = GameObject.Find ("Player Two Container");
			}


			SceneManager.sceneLoaded += _CompleteSceneTransition;
						
			DontDestroyOnLoad (gameObject);
			DontDestroyOnLoad (PlayerOne);
			DontDestroyOnLoad (PlayerTwo);
			DontDestroyOnLoad (SoundManager);
		}

		else
			Destroy (this.gameObject);
	}

	// Initialization:
	// public void Start () {
	// 	/*	COMMENTED OUT FOR DEBUG PURPOSES:
	// 	CurrentPlayer = PlayerOne;
	// 	*/
	// }


	void Update () {
		switch (_activeSceneName) {
			case "Main Menu":

				break;

			case "Gameboard":
				if (UIManager.TitleCardIsShowing) {
					if (!UIManager.TitleCardLock) {
						UIManager.TitleCardTimerUpdate (_titleTimeout);
						_titleTimeout -= Time.deltaTime;

						if (_titleTimeout < 0)
							UIManager.ToggleTitleCardActivation (false);
					}
				}
				
				else {
					if (Phase > 2) {
						UIManager.TimerUpdate (_timerValue);
						_timerValue -= Time.deltaTime;

						if (_timerValue < 0)
							SwitchTurns ();
					}
					
				}
				break;
		}
	}



	// For switching turns in the manager, which will in turn be called by Graphics, etc.
	public void SwitchTurns() {
		// Return to main menu if game over.
		if (GameOver) {
			UIManager.WinTitleCard (_winner.Name);
			GameManagerScript.Instance.SoundManager.PlaySound ("Gameover");
			_winner.Score++;
		}
		
		else {
			if (CurrentPlayer == PlayerTwo) {
				if (Phase < 3)
					Phase++;
				else
					Turn++;
			}

			if (CurrentPlayer.Value > 0)
				CurrentPlayer = PlayerTwo;
			

			else
				CurrentPlayer = PlayerOne;

			Gameboard.CurrentState = GameboardScript.GameboardState.NORMAL;
			Gameboard.CurrentUnitStructureAsset = UnitStructure.UnitStructureType.NONE;
			
			if (Phase > 2)
				Gameboard.ResetUnitMovement ();

			// Resets the _hasPlacedSwarmUnit bool to allow for additional placement.
			CurrentPlayer.ResetSwarmLock ();

			_titleTimeout = TitleCardTimeoutMax;		
			UIManager.SwitchSides ();
			Gameboard.Clear ();
			_timerValue = TimerMax;
		}
	}



	// Called when the MainMenuManager submits the setup for their character.
	public void GameSetup () {
		// PlayerOne.CopyPlayerData (players[0]);
		// PlayerTwo.CopyPlayerData (players[1]);

		PlayerOne.UnitStructureContainer = GameObject.Find ("Player One Container");
		PlayerTwo.UnitStructureContainer = GameObject.Find ("Player Two Container");

		CurrentPlayer = PlayerOne;
		Debug.Log ("Player Data loaded successfully");
	}


	
	// Called by InputManager when user presses the A button when 
	public void EndGame () {
		UIManager.TitleCardLock = false;
		SceneManager.LoadScene ("Main Menu");
	}





	public void ResetData (bool isHardReset = false, bool isFromMainMenu = false) {
		if (PlayerOne.GetUnitStructureCount () > 0)
			foreach (Transform playerOneUS in PlayerOne.UnitStructureContainer.GetComponentInChildren<Transform> ())
					Destroy (playerOneUS.gameObject);

		if (PlayerTwo.GetUnitStructureCount () > 0)
			foreach (Transform playerTwoUS in PlayerTwo.UnitStructureContainer.GetComponentInChildren<Transform> ())
				Destroy (playerTwoUS.gameObject);

		if (isHardReset) {
			PlayerOne = Instantiate (BASE_PLAYER_ONE);
			PlayerOne.Initialize ();
			PlayerOne.UnitStructureContainer = GameObject.Find ("Player One Container");

			PlayerTwo = Instantiate (BASE_PLAYER_TWO);
			PlayerTwo.Initialize ();
			PlayerTwo.UnitStructureContainer = GameObject.Find ("Player Two Container");

			PlayerOne.FullResetData ();
			PlayerTwo.FullResetData ();
		}
		
		else {
			PlayerOne.ResetData (isFromMainMenu);
			PlayerTwo.ResetData (isFromMainMenu);
		}

		_winner = null;
		Phase = 1;
		Turn = 0;
		_timerValue = TimerMax;
		
		if (!isFromMainMenu)
			EndGame ();
	}



	// Called to make the connections to between the GameManager Instance and the 'Gameboard' scene on the Scene.Load () call from the main menu.
	private void _CompleteSceneTransition (Scene scene, LoadSceneMode mode) {
		DebugOutput ("GameManagerScript:\tProcessing additional scene transition instuctions.");		

		_activeSceneName = scene.name;

		// Transition: Gameboard -> Main Menu
		// n/a

		// Transition: Main Menu -> Gameboard
		if (_activeSceneName == "Gameboard") {
			CurrentPlayer = PlayerOne;
			PlayerOne.Initialize ();
			PlayerTwo.Initialize ();
			Gameboard = GameObject.Find ("Gameboard").GetComponent<GameboardScript> ();
			UIManager = GameObject.Find ("UI Manager").GetComponent<UIManagerScript> ();
			CombatManager = GameObject.Find ("Combat Manager").GetComponent<CombatManagerScript> ();
			InputManager.LoadGameboardTileContainer (Gameboard.Container);
			PlayerOne.UnitStructureContainer = Gameboard.PlayerOneUnitStructureContainer;
			PlayerTwo.UnitStructureContainer = Gameboard.PlayerTwoUnitStructureContainer;			
		}

		SoundManager.PlayTrack (_activeSceneName);
	}



	private bool _GameOver () {
		
		/*
			Win Conditions:
		----------------------------
		- Destroy all non-STRUCTURE type UnitStructures.
		- Player landed in the conduit.
		*/
		if ((PlayerOne.GetUnitCount () <= 0) && (Phase >= 3))
			_winner = PlayerTwo;

		else if ((PlayerTwo.GetUnitCount () <= 0)  && (Phase >= 3))
			_winner = PlayerOne;

		else if (ConduitEntered)
			_winner = CurrentPlayer;

		return (_winner != null);
	}



}
