﻿/*
 *	FILE:	GameboardScript.cs
 *	AUTHOR:	mmiddleton
 *	DATE:	7 SEP 2017
 * 
 * 	DESCRIPTION:
 * 	This script is to be connected to the GAMEBOARD Empty. It will hold the code for the gameboard, movement, [more to be added?].
 * 
 * 	REFERENCED FROM:
 * 	n/a
 */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GameboardScript : MonoBehaviour {
	// Enums & Get-Sets
	public enum GameboardState {NORMAL, PLACEMENT, MOVEMENT, TOWER_ATTACK, TRAP_ATTACK, CONFLICT_RESOLUTION};
	public GameObject Container { get {return _tileContainer;} }


	// Public Variables:
	public UnitStructure.UnitStructureType CurrentUnitStructureAsset;
	public GameboardState CurrentState;
	public const int Width = 18;
	public const int Height = 12;
	public float TileHeight;
	public float TileWidth;
	public float UnitStructureOffsetX;
	public float UnitStructureOffsetY;
	public GameObject PlayerOneUnitStructureContainer;
	public GameObject PlayerTwoUnitStructureContainer;



	// Private Variables - Manage these in-engine
	[SerializeField]
	private GameObject _tilePrefab;
	[SerializeField]
	private GameObject _tileContainer;
	[SerializeField]
	private float _tileYOffset;
	[SerializeField]
	private GridLayoutGroup _gameboardLayout;

	

	// Local Variables:
	private GameboardTileScript _sourceTile;
	private SpriteRenderer _gameboardSpriteRenderer;  // Visualization of GameBoard. (Likely to be removed when real assets come in.)
	private char[,] INITIAL_BOARD = new char[Height, Width] {
		{'-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
		{'-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
		{'-', '-', 'X', 'X', 'X', '-', '-', '-', '-', '-', '-', '-', '-', 'X', 'X', 'X', '-', '-'},
		{'-', '-', 'X', 'X', 'X', '-', '-', '-', '-', '-', '-', '-', '-', 'X', 'X', 'X', '-', '-'},
		{'-', '-', 'X', 'X', 'X', '-', '-', '-', '-', '-', '-', '-', '-', 'X', 'X', 'X', '-', '-'},
		{'C', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', 'c'},
		{'C', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', 'c'},
		{'-', '-', 'X', 'X', 'X', '-', '-', '-', '-', '-', '-', '-', '-', 'X', 'X', 'X', '-', '-'},
		{'-', '-', 'X', 'X', 'X', '-', '-', '-', '-', '-', '-', '-', '-', 'X', 'X', 'X', '-', '-'},
		{'-', '-', 'X', 'X', 'X', '-', '-', '-', '-', '-', '-', '-', '-', 'X', 'X', 'X', '-', '-'},
		{'-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-'},
		{'-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-'}
	};
	/*
		Character Breakdown:
		--------------------------
		> Upper Case characters = Player One
		> Lower Case Characters = Player Two
		> Cc = Conduit
		> Tt = Tower
		> Qq = Trap
		> Ww = Wall
		> Aa = Attack Unit
		> Dd = Defense Unit
		> Ss = Swarm Unit
		> X = Plateau
		> O = Open Space
	*/


	// Use this for initialization
	void Start () {
		RectTransform tileContainerRectTransform = _tileContainer.GetComponent<RectTransform> ();		
		_gameboardLayout.cellSize = new Vector2 (tileContainerRectTransform.rect.width / Width, tileContainerRectTransform.rect.height / Height);
		TileWidth = _gameboardLayout.cellSize.x;
		TileHeight = _gameboardLayout.cellSize.y;

		// Align the displayed map to the array.
		// _gameboardSpriteRenderer = gameObject.GetComponent<SpriteRenderer> ();

		// Initialize the GameboardPosition array:
		_CreateGameboard ();

		GameManagerScript.Instance.FocusTile = _tileContainer.transform.GetChild (0).GetComponent <GameboardTileScript> ();
	}
		


	// Generates the name of the sprite to use for GameboardTileScript.
	public void SetCurrentUnitStructureType (UnitStructure.UnitStructureType type) {
		CurrentState = GameboardState.PLACEMENT;
		CurrentUnitStructureAsset = type;
		GameManagerScript.Instance.UIManager.AppendTextToTerminal ("\n\n\nblitzsiege$  ./create_unit " + type.ToString ().ToUpper () + " > ./place_unit");
		
	
		// Highlighting:	//////////////////////////////////////////////////////////////////////////////////////////////
		_tileContainer.BroadcastMessage ("DeactivateHighlight");

		if (CurrentUnitStructureAsset == UnitStructure.UnitStructureType.TOWER) {
			if (!GameManagerScript.Instance.CurrentPlayer.CanPlaceTower())
				_tileContainer.BroadcastMessage ("ActivateTowerHighlight");
		}

		else
			_tileContainer.BroadcastMessage ("ActivateGeneralHighlight");
		// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
		GameManagerScript.Instance.FocusTile.SendMessage ("SetFocus");
	}



	// Adds a new UnitStructure to the Gameboard (INFO: Called from GameboardTileScript.)
	public void AddNewUnitStructure (GameObject toBeAdded) {
		// NOTETOSELF
		toBeAdded.transform.SetParent (GameManagerScript.Instance.CurrentPlayer.UnitStructureContainer.transform);
		CurrentState = GameboardState.NORMAL;
		CurrentUnitStructureAsset = UnitStructure.UnitStructureType.NONE;
		_tileContainer.BroadcastMessage ("DeactivateHighlight");
	}



	// Sends "EnableSelectable" message to the diamond around the UnitStructure.
	public void EnableSelectableAroundTile (GameboardTileScript selectedTile) {
		// CurrentState = GameboardState.MOVEMENT;
		int relevantIndex = (int)(selectedTile.Position.x + (selectedTile.Position.y * Width));
		
		// Send Highlight Messages:
		if (selectedTile.Position.x > 0)													// LEFT 1
			_tileContainer.transform.GetChild (relevantIndex - 1).SendMessage ("EnableSelectable");

		if (selectedTile.Position.x > 1)													// LEFT 2
			_tileContainer.transform.GetChild (relevantIndex - 2).SendMessage ("EnableSelectable");

		if (selectedTile.Position.x > 0 && selectedTile.Position.y > 0)						// UP LEFT
			_tileContainer.transform.GetChild (relevantIndex - Width - 1).SendMessage ("EnableSelectable");

		if (selectedTile.Position.y > 0)													// UP 1
			_tileContainer.transform.GetChild (relevantIndex - Width).SendMessage ("EnableSelectable");

		if (selectedTile.Position.y > 1)													// UP 2
			_tileContainer.transform.GetChild (relevantIndex - (2 * Width)).SendMessage ("EnableSelectable");

		if (selectedTile.Position.x < Width - 1 && selectedTile.Position.y > 0)				// UP RIGHT
			_tileContainer.transform.GetChild (relevantIndex - Width + 1).SendMessage ("EnableSelectable");

		if (selectedTile.Position.x < Width - 1)											// RIGHT 1
			_tileContainer.transform.GetChild (relevantIndex + 1).SendMessage ("EnableSelectable");

		if (selectedTile.Position.x < Width - 2)											// RIGHT 2
			_tileContainer.transform.GetChild (relevantIndex + 2).SendMessage ("EnableSelectable");

		if (selectedTile.Position.x < Width - 1 && selectedTile.Position.y < Height - 1)	// DOWN RIGHT
			_tileContainer.transform.GetChild (relevantIndex + Width + 1).SendMessage ("EnableSelectable");

		if (selectedTile.Position.y < Height - 1)											// DOWN 1
			_tileContainer.transform.GetChild (relevantIndex + Width).SendMessage ("EnableSelectable");

		if (selectedTile.Position.y < Height - 2)											// DOWN 2
			_tileContainer.transform.GetChild (relevantIndex + (2 * Width)).SendMessage ("EnableSelectable");

		if (selectedTile.Position.x > 0 && selectedTile.Position.y < Height - 1)			// DOWN LEFT
			_tileContainer.transform.GetChild (relevantIndex + Width - 1).SendMessage ("EnableSelectable");
		//


		_sourceTile = selectedTile;
		_tileContainer.BroadcastMessage ("ActivateMovementHighlight");
	}


	public void DisableSelectableAroundTile (GameboardTileScript selectedTile) {
		// CurrentState = GameboardState.MOVEMENT;
		int relevantIndex = (int)(selectedTile.Position.x + (selectedTile.Position.y * Width));
		
		// Send Highlight Messages:
		if (selectedTile.Position.x > 0)													// LEFT 1
			_tileContainer.transform.GetChild (relevantIndex - 1).SendMessage ("DisableSelectable");

		if (selectedTile.Position.x > 1)													// LEFT 2
			_tileContainer.transform.GetChild (relevantIndex - 2).SendMessage ("DisableSelectable");

		if (selectedTile.Position.x > 0 && selectedTile.Position.y > 0)						// UP LEFT
			_tileContainer.transform.GetChild (relevantIndex - Width - 1).SendMessage ("DisableSelectable");

		if (selectedTile.Position.y > 0)													// UP 1
			_tileContainer.transform.GetChild (relevantIndex - Width).SendMessage ("DisableSelectable");

		if (selectedTile.Position.y > 1)													// UP 2
			_tileContainer.transform.GetChild (relevantIndex - (2 * Width)).SendMessage ("DisableSelectable");

		if (selectedTile.Position.x < Width - 1 && selectedTile.Position.y > 0)				// UP RIGHT
			_tileContainer.transform.GetChild (relevantIndex - Width + 1).SendMessage ("DisableSelectable");

		if (selectedTile.Position.x < Width - 1)											// RIGHT 1
			_tileContainer.transform.GetChild (relevantIndex + 1).SendMessage ("DisableSelectable");

		if (selectedTile.Position.x < Width - 2)											// RIGHT 2
			_tileContainer.transform.GetChild (relevantIndex + 2).SendMessage ("DisableSelectable");

		if (selectedTile.Position.x < Width - 1 && selectedTile.Position.y < Height - 1)	// DOWN RIGHT
			_tileContainer.transform.GetChild (relevantIndex + Width + 1).SendMessage ("DisableSelectable");

		if (selectedTile.Position.y < Height - 1)											// DOWN 1
			_tileContainer.transform.GetChild (relevantIndex + Width).SendMessage ("DisableSelectable");

		if (selectedTile.Position.y < Height - 2)											// DOWN 2
			_tileContainer.transform.GetChild (relevantIndex + (2 * Width)).SendMessage ("DisableSelectable");

		if (selectedTile.Position.x > 0 && selectedTile.Position.y < Height - 1)			// DOWN LEFT
			_tileContainer.transform.GetChild (relevantIndex + Width - 1).SendMessage ("DisableSelectable");
		//


		_sourceTile = selectedTile;
	}


	public void DisableAllSelectable () {
		_tileContainer.BroadcastMessage ("DisableSelectable");
	}



	public void Clear () {
		_tileContainer.BroadcastMessage ("DisableSelectable");
		_tileContainer.BroadcastMessage ("DeactivateHighlight");
	}


	public void MoveUnitToTile (GameboardTileScript target) {
		if (target.IsSelectable) {
			if (target.IsNotOccupied () && !_sourceTile.PeakResidentUnitStructure ().IsStructure)
				target.MoveResident (_sourceTile.PopResidentUnitStructure ());
		
			else if (target.ResidentIsEnemy ())
				target.InitiateCombat (_sourceTile.PeakResidentUnitStructure ());
		}

		else
			Debug.Log ("ERROR: Unit cannot move to that position.");
		
		CurrentState = GameboardState.NORMAL;
		_tileContainer.BroadcastMessage ("DisableSelectable");
		_tileContainer.BroadcastMessage ("DeactivateHighlight");
	}



	public void SelectStructureAction (UnitStructure structure) {
		switch (structure.Type) {
			case UnitStructure.UnitStructureType.TOWER:
				CurrentState = GameboardState.TOWER_ATTACK;
				break;

			case UnitStructure.UnitStructureType.TRAP:
				CurrentState = GameboardState.TRAP_ATTACK;
				break;

			default:
				break;				
		}
	}




	// Resets the 'Focus' tile to the starting point for the corresponding player.
	public void ResetFocusPosition () {
		_tileContainer.BroadcastMessage ("DeactivateHighlight");
	}


	// Brodacasts a message to all UnitStructures in the containers to reset HasMoved bool.
	public void ResetUnitMovement () {
		if (GameManagerScript.Instance.PlayerOne.GetUnitStructureCount () > 0)
			PlayerOneUnitStructureContainer.BroadcastMessage ("ResetMovement");

		if (GameManagerScript.Instance.PlayerTwo.GetUnitStructureCount () > 0)
			PlayerTwoUnitStructureContainer.BroadcastMessage ("ResetMovement");
	}


	public bool CheckSwarmIsUninvaded () {
		Player opponent;
		if (GameManagerScript.Instance.CurrentPlayer == GameManagerScript.Instance.PlayerOne)
			opponent = GameManagerScript.Instance.PlayerTwo;

		else
			opponent = GameManagerScript.Instance.PlayerOne;

		foreach (UnitStructure us in opponent.UnitStructureContainer.GetComponentsInChildren<UnitStructure> ())
			if (us.CurrentTile.PlayerSide == GameManagerScript.Instance.CurrentPlayer.Value)
				return false;
		
		return true;
	}



	// Adjusts the Gameboard Sprite to stretch properly to the correct width to fill the screen.
	private void _CreateGameboard () {
		Vector3 startPosition = Camera.main.ScreenToWorldPoint (new Vector3 (0, Screen.height * _tileYOffset)); // Make room for the background/Scoreboard
		GameObject aTile;
		GameboardTileScript tileScript;
		BoxCollider2D tileCollider;
		RectTransform tileRect;

		for (int y = 0; y < Height; y++)
			for (int x = 0; x < Width; x++) {
				// Initialize Tile:
				aTile = Instantiate (_tilePrefab, _tileContainer.transform);
				aTile.name = ("gameboard_tile " + (x+1) + "," + (y+1));
				tileRect = aTile.GetComponent<RectTransform> ();
				

				// Modify the TileScript Component:
				tileScript = aTile.GetComponent<GameboardTileScript> ();
				tileScript.Position = new Vector2 (x, y);
				tileScript.PlayerSide = (x < Width/2) ? (1) : (-1); // If on left, belongs to P1, else P2.
				tileScript.TileImage.sprite = GameManagerScript.Instance.UIManager.LoadTileSprite (INITIAL_BOARD[y, x]);
				tileScript.TileID = (y * Width) + x; 

				if (INITIAL_BOARD[y, x] == 'X')
					tileScript.IsPlateau = true;

				else if (INITIAL_BOARD[y, x] == 'C' || INITIAL_BOARD[y, x] == 'c')
					tileScript.IsConduit = true;


				// Modify the Box Collider Component;
				tileCollider = aTile.GetComponent<BoxCollider2D> ();
				tileCollider.size = tileRect.rect.size;
				tileCollider.offset = new Vector2 (tileRect.rect.width / 2, -(tileRect.rect.height));
			}
	}
}
