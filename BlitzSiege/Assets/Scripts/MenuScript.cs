﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MenuScript : MonoBehaviour {
	public enum MenuDirection {UP = -1, DOWN = 1, LEFT = 2, RIGHT = 3};
	public abstract void Load ();
	public abstract void SelectCurrentFocus ();
	public abstract void SetCurrentFocus (MenuDirection direction);
}
