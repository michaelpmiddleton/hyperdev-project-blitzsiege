﻿/*
 *	FILE:	InputManagerScript.cs
 *	AUTHOR:	mmiddleton
 *	DATE:	19 OCT 2017
 * 
 * 	DESCRIPTION:
 * 	This script is to be connected to the GAMEMANAGER empty. It will map and connect gamepad input to the game.
 * 
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InputManagerScript : MonoBehaviour {
	// Enums:
	public enum PlatformType { UNSUPPORTED, WINDOWS, MAC, LINUX };
	public bool BlockKeyboardInput { get { return _blockKeyboardInput; } set { _blockKeyboardInput = value; } }

	// Public Variables:
	public PlatformType CurrentPlatform;

	// Private Variables - Manage these in-engine:
	[SerializeField]
	private GameObject _tileContainer;
	[SerializeField]
	private float _verticalAxisSensitivity;
	[SerializeField]
	private float _horizontalAxisSensitivity;
	[SerializeField]
	private float _windowsInputDelay;
	
	

	// Private Variables:
	private Vector2 _playerOneFocusPoint;
	private Vector2 _playerTwoFocusPoint;
	private bool _dPadIsAxis;
	private float _horizonalDelayTimer;
	private float _verticalDelayTimer;
	[SerializeField]
	private bool _blockKeyboardInput;

	private string _aButton,
		_bButton,
		_xButton,
		_yButton,
		_leftBumper,
		_rightBumper,
		_startButton,
		_backButton,
		_dPadUp,
		_dPadDown,
		_dPadLeft,
		_dPadRight;
	// ///////////////////////////////////



	void Awake () {
		switch (Application.platform) {
			case RuntimePlatform.OSXEditor:
			case RuntimePlatform.OSXPlayer:
				CurrentPlatform = PlatformType.MAC;
				break;

			case RuntimePlatform.LinuxEditor:
			case RuntimePlatform.LinuxPlayer:
				CurrentPlatform = PlatformType.LINUX;
				break;

			case RuntimePlatform.WindowsEditor:
			case RuntimePlatform.WindowsPlayer:
				CurrentPlatform = PlatformType.WINDOWS;
				break;

			default:
				CurrentPlatform = PlatformType.UNSUPPORTED;
				GameManagerScript.DebugOutput ("CRITICAL ERROR:\tUser is using currently unsupported device!");
				break;
		}
		
		_LoadInputMapping ();

		_playerOneFocusPoint = new Vector2 (1, (int)(GameboardScript.Height * 0.5f)-1);
		_playerTwoFocusPoint = new Vector2 (GameboardScript.Width - 2, _playerOneFocusPoint.y);
	}

	void Update () {
		_ProcessVertical ();
		_ProcessHorizontal ();
		_ProccessAButton ();
		_ProccessBButton ();
		_ProccessXButton ();
		_ProccessYButton ();
		_ProcessLeftBumper ();
		_ProcessRightBumper ();
		_ProccessStartButton ();
		_ProcessBackButton ();
		_ProcessDebugReset ();
	}


	public void LoadGameboardTileContainer (GameObject container) {
		_tileContainer = container;
	}


	/*
		/////////////////////////////////////////////////////////////////////////////////////
		These methods are for proccessing requests from the gamepad using various conditions
		found in other objects.
		/////////////////////////////////////////////////////////////////////////////////////
	*/

	//	A Button	-	Confirm, Submit, Select
	void _ProccessAButton () {
		if (Input.GetKeyUp (_aButton) || Input.GetButtonUp ("Submit")) {
			switch (SceneManager.GetActiveScene ().name) {
				case "Main Menu":
					if (!BlockKeyboardInput)
						MainMenuManagerScript.Instance.SelectCurrentMenuItem ();
					break;

				case "Gameboard":
					// TitleCard interaction:
					if (GameManagerScript.Instance.UIManager.TitleCardIsShowing) {
						if (GameManagerScript.Instance.GameOver)
							GameManagerScript.Instance.ResetData ();
						
						else {
							Vector2 focusPoint = (GameManagerScript.Instance.CurrentPlayer.Value == 1) ? (_playerOneFocusPoint) : (_playerTwoFocusPoint);
							_SetNewFocus ((int)(focusPoint.x) + (int)(focusPoint.y * GameboardScript.Width));
							GameManagerScript.Instance.UIManager.ToggleTitleCardActivation (false);
						}
					}

					// Other stuff.
					else {
						GameManagerScript.Instance.FocusTile.OnMouseDown ();
						GameManagerScript.Instance.UIManager.TerminalDefault ();
					}
					break;
			}
		}
	}



	// B Button		-	Choose Wall, Choose Defense Unit, Cancel
	void _ProccessBButton () {
		if ((Input.GetKeyUp (_bButton) || Input.GetButtonUp ("Fire3")))  {
			switch (SceneManager.GetActiveScene ().name) {
				case "Main Menu":
					if (!BlockKeyboardInput)
						MainMenuManagerScript.Instance.BackMenu ();
					break;

				case "Gameboard":
					if (!GameManagerScript.Instance.UIManager.TitleCardIsShowing) {
						if (GameManagerScript.Instance.Gameboard.CurrentState == GameboardScript.GameboardState.NORMAL)
							switch (GameManagerScript.Instance.Phase) {
								case 1:
									GameManagerScript.DebugOutput ("InputManager:\tTOWER selected for PLACEMENT.");
									GameManagerScript.Instance.Gameboard.SetCurrentUnitStructureType (UnitStructure.UnitStructureType.TOWER);
									break;

								case 2:
									GameManagerScript.DebugOutput ("InputManager:\tDEFENSE selected for PLACEMENT.");
									GameManagerScript.Instance.Gameboard.SetCurrentUnitStructureType (UnitStructure.UnitStructureType.DEFENSE);
									break;

								case 3:
									GameManagerScript.DebugOutput ("InputManager:\tTOWER selected for ACTIVATION.");
									GameManagerScript.DebugOutput ("TODO");
									break;

								default:
									Debug.LogError ("InputManager._ProcessYButton\tGame is in unsupported phase, Y BUTTON PRESSED.");
									break;
							}
						
						else {
							GameManagerScript.Instance.UIManager.TerminalDefault ();
							GameManagerScript.Instance.Gameboard.CurrentState = GameboardScript.GameboardState.NORMAL;
							GameManagerScript.Instance.Gameboard.CurrentUnitStructureAsset = UnitStructure.UnitStructureType.NONE;
							_tileContainer.BroadcastMessage ("DisableSelectable");
							_tileContainer.BroadcastMessage ("DeactivateHighlight");
						}
					}
					break;
			}
		}
	}



	// X Button		-	Choose Trap, Choose Attack Unit
	void _ProccessXButton () {
		if ((Input.GetKeyUp (_xButton) || Input.GetButtonUp ("Fire1"))) {
			switch (SceneManager.GetActiveScene ().name) {
				case "Main Menu":

					break;

				case "Gameboard":
					if (!GameManagerScript.Instance.UIManager.TitleCardIsShowing)
						switch (GameManagerScript.Instance.Phase) {
							case 1:
								GameManagerScript.DebugOutput ("InputManager:\tTRAP selected for PLACEMENT.");
								GameManagerScript.Instance.Gameboard.SetCurrentUnitStructureType (UnitStructure.UnitStructureType.TRAP);
								break;

							case 2:
								GameManagerScript.DebugOutput ("InputManager:\tATTACK selected for PLACEMENT.");
								GameManagerScript.Instance.Gameboard.SetCurrentUnitStructureType (UnitStructure.UnitStructureType.ATTACK);
								break;

							case 3:
								GameManagerScript.DebugOutput ("InputManager:\tTRAP selected for ACTIVATION.");
								GameManagerScript.DebugOutput ("TODO");
								break;

							default:
								Debug.LogError ("InputManager._ProcessXButton\tGame is in unsupported phase, X BUTTON PRESSED.");
							break;
						}
					break;
			}
		}
	}



	// Y Button		-	Choose Tower, Choose Swarm Unit
	void _ProccessYButton () {
		if ((Input.GetKeyUp (_yButton) || Input.GetButtonUp ("Fire2"))) {
			switch (SceneManager.GetActiveScene ().name) {
				case "Main Menu":

					break;

				case "Gameboard":
					if (!GameManagerScript.Instance.UIManager.TitleCardIsShowing)
						switch (GameManagerScript.Instance.Phase) {
							case 1:
								GameManagerScript.Instance.Gameboard.SetCurrentUnitStructureType (UnitStructure.UnitStructureType.WALL);
								break;

							case 2:
								if (GameManagerScript.Instance.CurrentPlayer.CanPlaceSwarmUnit ())
									GameManagerScript.Instance.Gameboard.SetCurrentUnitStructureType (UnitStructure.UnitStructureType.SWARM);
								break;

							case 3:
								if (GameManagerScript.Instance.CurrentPlayer.CanPlaceSwarmUnit ())
									GameManagerScript.Instance.Gameboard.SetCurrentUnitStructureType (UnitStructure.UnitStructureType.SWARM);
								break;

							default:
								Debug.LogError ("InputManager._ProcessYButton\tGame is in unsupported phase, Y BUTTON PRESSED.");
								break;
						}
						break;
			}
		}
	}



	// Left bumper -	Snap to the previous UnitStructure in their container.
	void _ProcessLeftBumper () {
		if ((Input.GetKeyUp (_leftBumper) || Input.GetButtonUp ("LeftBumper")))
			switch (SceneManager.GetActiveScene ().name) {
				case "Main Menu":
					MainMenuManagerScript.Instance.SwitchFocusPlayer (1);
					break;

				case "Gameboard":
					if (GameManagerScript.Instance.Phase > 1) {
						GameManagerScript.Instance.Gameboard.CurrentState = GameboardScript.GameboardState.NORMAL;												
						UnitStructure potentialResident = GameManagerScript.Instance.FocusTile.PeakResidentUnitStructure();
						int snapToIndex;

						if (potentialResident != null)
							snapToIndex = Mathf.Abs(potentialResident.transform.GetSiblingIndex()+potentialResident.transform.parent.childCount - 1) % potentialResident.transform.parent.childCount;

						else			
							snapToIndex = 0;	

							GameboardTileScript snapToTile = GameManagerScript.Instance.CurrentPlayer.UnitStructureContainer.transform.GetChild (snapToIndex).GetComponent<UnitStructure> ().CurrentTile;
							GameManagerScript.Instance.Gameboard.DisableAllSelectable ();
							GameManagerScript.Instance.Gameboard.DisableSelectableAroundTile (GameManagerScript.Instance.FocusTile);
							_SetNewFocus (snapToTile.TileID);
							GameManagerScript.Instance.FocusTile.OnMouseDown ();						
					}
					break;
			}
	}


	// Right bumper	-	Snap to the next UnitStructure in their container.
	void _ProcessRightBumper () {
		if ((Input.GetKeyUp (_rightBumper) || Input.GetButtonUp ("RightBumper")))
			switch (SceneManager.GetActiveScene ().name) {
				case "Main Menu":
					MainMenuManagerScript.Instance.SwitchFocusPlayer (2);
					break;

				case "Gameboard":
						GameManagerScript.Instance.Gameboard.CurrentState = GameboardScript.GameboardState.NORMAL;
						if (GameManagerScript.Instance.Phase > 1) {
							UnitStructure potentialResident = GameManagerScript.Instance.FocusTile.PeakResidentUnitStructure();
							int snapToIndex;

							if (potentialResident != null)
								snapToIndex = Mathf.Abs(potentialResident.transform.GetSiblingIndex () + 1) % potentialResident.transform.parent.childCount;

							else			
								snapToIndex = 0;	

							GameboardTileScript snapToTile = GameManagerScript.Instance.CurrentPlayer.UnitStructureContainer.transform.GetChild (snapToIndex).GetComponent<UnitStructure> ().CurrentTile;
							GameManagerScript.Instance.Gameboard.DisableAllSelectable ();
							GameManagerScript.Instance.Gameboard.EnableSelectableAroundTile (snapToTile);
							_SetNewFocus (snapToTile.TileID);
							GameManagerScript.Instance.FocusTile.OnMouseDown ();
						}
					break;
			}
	}


	// Start Button	-	Menu Button
	void _ProccessStartButton () {
		if ((Input.GetKeyUp (_startButton) || Input.GetButtonUp ("Cancel")))
			switch (SceneManager.GetActiveScene ().name) {
				case "Main Menu":
					if (!BlockKeyboardInput)
						MainMenuManagerScript.Instance.StartGame ();
					
					break;

				case "Gameboard":
					GameManagerScript.DebugOutput ("Start button pressed.");
					break;
			}
	}


	// Back Button -	End Turn Early
	void _ProcessBackButton () {
		if ((Input.GetKeyUp (_backButton) || Input.GetButtonUp ("Back")))
			switch (SceneManager.GetActiveScene ().name) {
				case "Main Menu":

					break;

				case "Gameboard":
					GameManagerScript.Instance.SwitchTurns ();
					break;
			}
	}


	// Vertical input - Move up or down in the given context.
	void _ProcessVertical () {
		float input = Input.GetAxisRaw ("Vertical");

		switch (SceneManager.GetActiveScene ().name) {
			case "Main Menu":
				if ((input < 0 - (_verticalAxisSensitivity) && _verticalDelayTimer < 0 ) || (!_dPadIsAxis && Input.GetKeyUp (_dPadUp))) {
					_verticalDelayTimer = _windowsInputDelay;
					MainMenuManagerScript.Instance.MoveInMenu (MenuScript.MenuDirection.UP);
				}

				else if ((input > _verticalAxisSensitivity && _verticalDelayTimer < 0)|| (!_dPadIsAxis && Input.GetKeyUp (_dPadDown))) {
					_verticalDelayTimer = _windowsInputDelay;
					MainMenuManagerScript.Instance.MoveInMenu (MenuScript.MenuDirection.DOWN);
				}
				break;

			case "Gameboard":
				if (!GameManagerScript.Instance.UIManager.TitleCardIsShowing) {
					GameboardTileScript newFocusTile = GameManagerScript.Instance.FocusTile;

					// Move to UP:
					if ((input < 0 - (_verticalAxisSensitivity) && _verticalDelayTimer < 0 ) || (!_dPadIsAxis && Input.GetKeyUp (_dPadUp))) {
						_verticalDelayTimer = _windowsInputDelay;

						if (newFocusTile.Position.y > 0) 
							_SetNewFocus (newFocusTile.gameObject.transform.GetSiblingIndex() - GameboardScript.Width);
					}

					// Move DOWN:
					else if ((input > _verticalAxisSensitivity && _verticalDelayTimer < 0)|| (!_dPadIsAxis && Input.GetKeyUp (_dPadDown))) {
						_verticalDelayTimer = _windowsInputDelay;

						if (newFocusTile.Position.y < GameboardScript.Height - 1) 
							_SetNewFocus (newFocusTile.gameObject.transform.GetSiblingIndex() + GameboardScript.Width);
					}

				}
				break;
		}

		_verticalDelayTimer -= Time.deltaTime;		
	}


	// Horizontal input - Move left or right in the given context.
	void _ProcessHorizontal () {
		float input = Input.GetAxisRaw ("Horizontal");

		switch (SceneManager.GetActiveScene ().name) {
			case "Main Menu":
				if ((input > _horizontalAxisSensitivity && _horizonalDelayTimer < 0) || (!_dPadIsAxis && Input.GetKeyUp (_dPadRight))) {
					_horizonalDelayTimer = _windowsInputDelay;
					MainMenuManagerScript.Instance.MoveInMenu (MenuScript.MenuDirection.RIGHT);	
				}
				else if ((input < 0 - _horizontalAxisSensitivity && _horizonalDelayTimer < 0)|| (!_dPadIsAxis && Input.GetKeyUp (_dPadLeft))) {
					_horizonalDelayTimer = _windowsInputDelay;
					MainMenuManagerScript.Instance.MoveInMenu (MenuScript.MenuDirection.LEFT);
				}
				break;

			case "Gameboard":
				if (!GameManagerScript.Instance.UIManager.TitleCardIsShowing) {
					GameboardTileScript newFocusTile = GameManagerScript.Instance.FocusTile;

					// Move RIGHT
					if ((input > _horizontalAxisSensitivity && _horizonalDelayTimer < 0) || (!_dPadIsAxis && Input.GetKeyUp (_dPadRight))) {
						_horizonalDelayTimer = _windowsInputDelay;
						
						// If OutOfMenu
						if (newFocusTile.Position.x < GameboardScript.Width - 1)
							_SetNewFocus (newFocusTile.transform.GetSiblingIndex() + 1);
						

						// Else, In Menu
					}

					// Move LEFT
					else if ((input < 0 - _horizontalAxisSensitivity && _horizonalDelayTimer < 0)|| (!_dPadIsAxis && Input.GetKeyUp (_dPadLeft))) {
						_horizonalDelayTimer = _windowsInputDelay;

						// If OutOfMenu
						if (newFocusTile.Position.x > 0)
							_SetNewFocus (newFocusTile.transform.GetSiblingIndex() - 1);
					

						// Else, In Menu
					}

				}
				break;
		}

		_horizonalDelayTimer -= Time.deltaTime;
	}


	void _ProcessDebugReset () {
		if (Input.GetButtonUp ("HardReset"))
			GameManagerScript.Instance.ResetData (true);

		else if (Input.GetButtonUp ("SoftReset"))
			GameManagerScript.Instance.ResetData ();
	}
	// //////////////////////////////////////////////////////////////////////////////////////



	void _LoadInputMapping () {
		// For mappings, go to:		http://wiki.unity3d.com/index.php?title=Xbox360Controller
		
		switch (CurrentPlatform) {
			case PlatformType.WINDOWS:
				_aButton = "joystick button 0";
				_bButton = "joystick button 1";
				_xButton = "joystick button 2";
				_yButton = "joystick button 3";
				_leftBumper = "joystick button 4";
				_rightBumper = "joystick button 5";
				_backButton = "joystick button 6";
				_startButton = "joystick button 7";
				_dPadIsAxis = true;
				break;

			case PlatformType.MAC:
				_aButton = "joystick button 16";
				_bButton = "joystick button 17";
				_xButton = "joystick button 18";
				_yButton = "joystick button 19";
				_leftBumper = "joystick button 13";
				_rightBumper = "joystick button 14";
				_backButton = "joystick button 10";
				_startButton = "joystick button 9";
				_dPadUp = "joystick button 5";
				_dPadDown = "joystick button 6";
				_dPadLeft = "joystick button 7";
				_dPadRight = "joystick button 8";
				_dPadIsAxis = false;
				break;

			default:
				Debug.LogWarning ("BlitzSiege does not support this Operating System! Controls may not work properly!");
				break;
		}
	}



	void _SetNewFocus (int position) {
		_tileContainer.BroadcastMessage ("DeactivateHighlight");

		switch (GameManagerScript.Instance.Gameboard.CurrentState) {
			case GameboardScript.GameboardState.PLACEMENT:
				if (GameManagerScript.Instance.Gameboard.CurrentUnitStructureAsset == UnitStructure.UnitStructureType.TOWER)
					_tileContainer.BroadcastMessage ("ActivateTowerHighlight");

				else
					_tileContainer.BroadcastMessage ("ActivateGeneralHighlight");

				break;

			case GameboardScript.GameboardState.MOVEMENT:
			case GameboardScript.GameboardState.TOWER_ATTACK:
			case GameboardScript.GameboardState.TRAP_ATTACK:			
					_tileContainer.BroadcastMessage ("ActivateMovementHighlight");
					break;

			default:

				break;
		}

	
		GameManagerScript.Instance.FocusTile = _tileContainer.transform.GetChild (position).GetComponent <GameboardTileScript> ();
		GameManagerScript.Instance.FocusTile.SendMessage ("SetFocus");
	}



}
