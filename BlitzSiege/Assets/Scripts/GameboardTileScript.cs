/*
 *	FILE:	GameboardTileScript.cs
 *	AUTHOR:	mmiddleton
 *	DATE:	14 SEP 2017
 * 
 * 	DESCRIPTION:
 * 	This script is to be connected to TILE prefab. It contains the position of the tile and whether or not the space is available for movement.
 * 
 * 	REFERENCED FROM:
 * 	- 'tile' prefab
 *	- GameboardScript
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameboardTileScript : MonoBehaviour {
	// Public Varriables:
	public bool IsPlateau; // True = Is part of Plateau, False = Not.
	public bool IsConduit; // True = Is one of the board's conduits, False = Not.
	public int PlayerSide;	// 1 if Player 1, -1 if player 2.
    public Vector2 Position; // {X, Y}
	public Image TileImage;
	public bool IsSelectable { get {return _canBeSelected;} set {_canBeSelected = value;}}
	public int TileID;

	// Private Variables - Manage these in-engine
	[SerializeField] 
	private GameObject _unitStructurePrefab;
	[SerializeField]
	private UnitStructure _residentUnitStructure;


	// Private Variables:
	private bool _isFocused;
	[SerializeField]
	private bool _canBeSelected;





	// Unity Basic Calls:
	void Awake () {
		TileImage = gameObject.GetComponent <Image> ();
	}



	void Start () {
		_residentUnitStructure = null;
	}



	// Instantiates a UnitStructureObject with the proper asset.
	// NOTETOSELF: Make sure the change to NORMAL appears in both TowerAttack & TrapAttack.

	public void OnMouseDown () {
		switch (GameManagerScript.Instance.Gameboard.CurrentState) {
			case GameboardScript.GameboardState.NORMAL:
				if (_residentUnitStructure != null && GameManagerScript.Instance.Phase > 2 && !ResidentIsEnemy ()) {
					if (!_residentUnitStructure.HasMoved) {
						// Structure
						if (_residentUnitStructure.IsStructure) 
							GameManagerScript.Instance.Gameboard.SelectStructureAction (_residentUnitStructure);
					
						// Unit
						else
							GameManagerScript.Instance.Gameboard.CurrentState = GameboardScript.GameboardState.MOVEMENT;


						if (_residentUnitStructure.Type != UnitStructure.UnitStructureType.WALL)
							GameManagerScript.Instance.Gameboard.EnableSelectableAroundTile (this);								
					}

					else
							GameManagerScript.DebugOutput ("OnMouseDown() [GameboardTileScript]:\tUnit has already moved!");
				}
				
				else
					GameManagerScript.DebugOutput ("OnMouseDown() [GameboardTileScript]:\tSelection is invalid.");

				break;

			case GameboardScript.GameboardState.PLACEMENT:
				GameManagerScript.DebugOutput ("GameboardTileScript():\tAttempting to place unit.");
				
				// NOTETOSELF: The rotation of characters needs to be reactivated/reworked when UI/UX Rework is complete.

				if (GameManagerScript.Instance.CurrentPlayer.CheckLegalPlacement (GameManagerScript.Instance.Gameboard.CurrentUnitStructureAsset, this)) {
					GameObject residentUnitStructureObject = Instantiate (_unitStructurePrefab);
					residentUnitStructureObject.name = GameManagerScript.Instance.Gameboard.CurrentUnitStructureAsset.ToString ();
					residentUnitStructureObject.GetComponent <SpriteRenderer> ().sprite = GameManagerScript.Instance.UIManager.LoadUnitStructureSprite (GameManagerScript.Instance.Gameboard.CurrentUnitStructureAsset);
					// residentUnitStructureObject.GetComponent <SpriteRenderer> ().flipY = (GameManagerScript.Instance.CurrentPlayer.Value == -1);
					residentUnitStructureObject.GetComponent <UnitStructure> ().Initialize (this);
					_residentUnitStructure = residentUnitStructureObject.GetComponent<UnitStructure> ();
					_residentUnitStructure.UpdateLayer ((int)Position.y);
					GameManagerScript.Instance.Gameboard.AddNewUnitStructure (residentUnitStructureObject);
					GameManagerScript.Instance.CurrentPlayer.LockSwarmPlacement ();
				}
				
				else		// TODO (Graphics solution to this problem)
					GameManagerScript.DebugOutput ("ERROR [GameboardTileScript.OnMouseDown] - User has attempted to place on an illegal position.");
		
				break;

			case GameboardScript.GameboardState.MOVEMENT:
				GameManagerScript.Instance.Gameboard.MoveUnitToTile (this);
				break;
		
			case GameboardScript.GameboardState.TOWER_ATTACK:
				GameManagerScript.Instance.Gameboard.MoveUnitToTile (this);
				break;

			case GameboardScript.GameboardState.TRAP_ATTACK:
				GameManagerScript.Instance.Gameboard.MoveUnitToTile (this);
				break;
		}
	}


	// Message Functions:	//////////////////////////////////////////////////
	void ActivateGeneralHighlight () {
		if (!IsPlateau)
			ActivateTowerHighlight ();
		
	}
	void ActivateTowerHighlight () {
		if (PlayerSide == GameManagerScript.Instance.CurrentPlayer.Value && _residentUnitStructure == null) {
			TileImage.color = GameManagerScript.Instance.UIManager.HighlightColor;
		}
	}
	void ActivateMovementHighlight () {
		if (!IsPlateau && _canBeSelected)
			TileImage.color = GameManagerScript.Instance.UIManager.HighlightColor;
	}
	void SetFocus () {
		TileImage.color = GameManagerScript.Instance.UIManager.FocusColor;
		_isFocused = true;
	}
	void DeactivateHighlight () {
		TileImage.color = Color.white;
	}
	public void DisableSelectable () {
		_canBeSelected = false;
	}
	public void EnableSelectable () {
		_canBeSelected = true;
	}
	// ////////////////////////////////////////////////////////////////////////



	// Returns true if there is no unit currently on THIS.
	public bool IsNotOccupied () {
		return _residentUnitStructure == null;
	}



	// Sets _residentUnitStructure to 'newResident' and adjusts its position to reflect its new tile-position.
	public void MoveResident (UnitStructure newResident) {
		newResident.transform.position = new Vector3 (
			transform.position.x + GameManagerScript.Instance.Gameboard.UnitStructureOffsetX,
			transform.position.y + GameManagerScript.Instance.Gameboard.UnitStructureOffsetY);
		_residentUnitStructure = newResident;
		_residentUnitStructure.CurrentTile = this;
		_residentUnitStructure.HasMoved = true;
		_residentUnitStructure.UpdateLayer ((int)Position.y);

		if (IsConduit)
			GameManagerScript.Instance.ConduitEntered = true;
	}



	// Runs the two units through the combat manager
	public void InitiateCombat (UnitStructure attacker) {
		bool defenderHasDied = GameManagerScript.Instance.CombatManager.Fight (attacker, _residentUnitStructure);

		// If there was a victor.
		if (defenderHasDied) {
			UnitStructure deadUnit = PopResidentUnitStructure ();
			deadUnit.Owner.KillUnit (deadUnit);
			
			if (!attacker.IsStructure)
				MoveResident (attacker);
		 }

		attacker.HasMoved = true;

		if (attacker.Type == UnitStructure.UnitStructureType.TRAP) {
			GameManagerScript.Instance.SoundManager.PlaySound ("Explosion");
			attacker.Owner.KillUnit (attacker, true);
		}
	}



	// Creates a copy of _residentUnitStructure, deletes the reference on THIS, and returns the copy.
	public UnitStructure PopResidentUnitStructure () {
		if (_residentUnitStructure != null) {
			UnitStructure toBeReturned = _residentUnitStructure;
			_residentUnitStructure = null;
			return toBeReturned;
		}

		else return null;
	}



	// Returns reference to _residentUnitStructure.
	public UnitStructure PeakResidentUnitStructure () {
		return _residentUnitStructure;
	}



	// Returns true if the resident unit structure is is owned by the current player.
	public bool ResidentIsEnemy () {
		return _residentUnitStructure.Owner != GameManagerScript.Instance.CurrentPlayer;
	}
}