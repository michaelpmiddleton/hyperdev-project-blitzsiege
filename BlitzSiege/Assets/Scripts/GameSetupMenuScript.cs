﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class GameSetupMenuScript : MenuScript {
	[SerializeField]
	private TextMeshProUGUI _currentlyEditingLabel;
	[SerializeField]
	private Text _currentlyEditingName;
	[SerializeField]
	private TextMeshProUGUI _flavorText;
	[SerializeField]
	private bool _lockText = false;
	[SerializeField]
	private Animator[] _animators;



	private string[] _flavorTextPresets = {
		"Hover over a faction to see its information.",
		"Fierce warriors at heart, the Engarans are unrivaled in offensive strength. Within their blood flows the blessing of Ara, the Breserker. This gift grants its recipients the skillset of its namesake -- deadly offense at the expense of a weaker defense. The Engarans fight to spread the teachings of their deity through the only method their faith allows: the ardour of lethal combat.",
		"Justicars of the great Republic of Plicat, the Plicatian Army is protected by thick and heavy armor. Nicknamed 'mobile fortresses,' each Plicatian soldier can take blow after blow without giving the faintest sign of fatigue. Though they are extremely difficult to topple, they emulate their kingdom's unaggressive disposition.",
		"No one really knows the origin of the Dominion. Some say they are former Plicatian soldiers turned mercenary after growing weary of the diplomatic and 'peaceful' strategies. Others say they are the more restrained members of the Engaran horde. One thing that is widely accepted however, they boast a difficult mix of both offensive and defensive tactics and strength."
	};
	private Player _currentlyEditing;
	



	public override void Load () {
		_flavorText.SetText (_flavorTextPresets[0]);
		_currentlyEditing = GameManagerScript.Instance.PlayerOne;

		foreach (Animator anim in _animators)
			anim.SetBool ("HasFocus", false);
	}

	public override void SetCurrentFocus (MenuDirection direction) {
	}

	public override void SelectCurrentFocus () {
	}

	public void DisplayFlavorTextForFaction (int factionIndex) {
		_flavorText.SetText (_flavorTextPresets[factionIndex]);
	}



	public void ChangeFocusPlayer (int value) {
		switch (value) {
			case 1:
				_currentlyEditing = GameManagerScript.Instance.PlayerOne;
				_currentlyEditingLabel.SetText (GameManagerScript.Instance.PlayerOne.Name);
				break;

			case 2:
				_currentlyEditing = GameManagerScript.Instance.PlayerTwo;
				_currentlyEditingLabel.SetText (GameManagerScript.Instance.PlayerTwo.Name);
				break;
		}
		

	}



	public void ToggleActiveFaction (int factionIndex) {
		foreach (Animator anim in _animators)
			anim.SetBool ("HasFocus", false);

		_animators[factionIndex - 1].SetBool ("HasFocus", true);
		_currentlyEditing.Faction = (GameManagerScript.Faction)System.Enum.Parse (typeof (GameManagerScript.Faction), factionIndex.ToString ());
		DisplayFlavorTextForFaction (factionIndex);

	}



	public void FinishPlayerNameEditing () {
		_currentlyEditing.Name = _currentlyEditingName.text;
		_currentlyEditingLabel.SetText (_currentlyEditingName.text);
		GameManagerScript.Instance.InputManager.BlockKeyboardInput = false;
	}



	private bool _IsValidPlayers () {
		return false;
	}
}
