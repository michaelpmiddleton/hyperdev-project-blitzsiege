/*
 *	FILE:	GameboardPosition.cs
 *	AUTHOR:	mmiddleton
 *	DATE:	14 SEP 2017
 * 
 * 	DESCRIPTION:
 * 	This script is to be used as object that holds both units & structrues on the gameboard as well.
 * 
 * 	REFERENCED FROM:
 * 	- GameManagerScript
 * 	- GameboardScript
 */
 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitStructure : MonoBehaviour {
    // Enums:
    public enum UnitStructureType {NONE = -1, ATTACK = 0, DEFENSE = 1, SWARM = 2, WALL = 3, TOWER = 4, TRAP = 5};
    

    // Public Variables:
    public UnitStructureType Type;          
    public Player Owner;
    public bool IsStructure;
    public bool HasMoved;
    public int AttackPower {get {return _offense;} set{}}
    public int DefensePower {get {return _defense;}set{}}
    public int CurrentHealth {get {return _health;}set{}}
    public int MaxHealth { get {return _maxHealth;}}
    public GameboardTileScript CurrentTile;

    

    // Private Variables - Manage these in-engine:
    [SerializeField]
    private HealthBarScript _healthBar;
    [SerializeField]
    private int _health;
    private int _maxHealth;

  

    // Private Variables:
    private RectTransform _rectTransform;
    private SpriteRenderer _spriteRenderer;
    [SerializeField]
    private int _offense;
    [SerializeField]
    private int _defense;



    // Called when creating a new UnitStructure.
    public void Initialize (GameboardTileScript currentTile) {
        Owner = GameManagerScript.Instance.CurrentPlayer;
        Type = GameManagerScript.Instance.Gameboard.CurrentUnitStructureAsset;
        CurrentTile = currentTile;
        HasMoved = false;
        _rectTransform = gameObject.GetComponent<RectTransform> ();
        _spriteRenderer = gameObject.GetComponent<SpriteRenderer> ();
        
        switch (Type) {
            case UnitStructureType.WALL:
            case UnitStructureType.TOWER:
            case UnitStructureType.TRAP:
                // Debug.Log ("UnitStructure:\tSTRUCTURE Object.");
                IsStructure = true;
                break;

            default:
                // GameManagerScript.DebugOutput ("Initialize [UnitStructure]:\tUnit is NOT a Structure - " + Type.ToString ());
                break;
        }

        _maxHealth = _health = GameManagerScript.Instance.CombatManager.GetHealthValue ();
        _offense = GameManagerScript.Instance.CombatManager.GetOffenseValue ();
        _defense = GameManagerScript.Instance.CombatManager.GetDefenseValue ();

        gameObject.transform.position = new Vector3 (
            CurrentTile.transform.position.x + GameManagerScript.Instance.Gameboard.UnitStructureOffsetX,
            CurrentTile.transform.position.y + GameManagerScript.Instance.Gameboard.UnitStructureOffsetY
        );
        
        _healthBar = (GameManagerScript.Instantiate (GameManagerScript.Instance.HealthBarPrefab)).GetComponent<HealthBarScript> ();
        _healthBar.transform.SetParent (transform);
        _healthBar.Initialize ();
    }

    public void ResetMovement () {
        HasMoved = false;
    }

    public void TakeDamage (int damage) {
        GameManagerScript.Instance.SoundManager.PlaySound ("Hit");
        _health -= damage;
        _healthBar.UpdateOutput ();
    }

    public bool HasDied () {
        return _health <= 0;
    }

    public void UpdateLayer (int newLayer) {
        _spriteRenderer.sortingOrder = newLayer;
        _healthBar.UpdateOrderInSortingLayer (newLayer);
    }
}