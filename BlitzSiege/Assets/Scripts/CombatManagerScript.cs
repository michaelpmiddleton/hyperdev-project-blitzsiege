﻿/*
 *	FILE:	CombatManager.cs
 *	AUTHOR:	mmiddleton
 *	DATE:	2 NOV 2017
 * 
 * 	DESCRIPTION:
 * 	This script is to be attached to an Empty in the GAMEBOARD scene. empty. It will handle all combat calculations for the game.
 * 
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatManagerScript : MonoBehaviour {
	private const int _ATTACK_MODIFIER = 2;
	private const int _DEFENSE_MODIFIER = 1;


	// Public Variables:
	// n/a


	// Private Variables - Manage these in-engine:
	[SerializeField]
	private int[] _OUSwarmOffense;
	[SerializeField]
	private int[] _OUSwarmDefense;
	[SerializeField]
	private int[] _DUOffense;
	[SerializeField]
	private int[] _DUDefense;
	[SerializeField]
	private int[] _structureOffenseStats;
	[SerializeField]
	private int[] _structureDefenseStats;
	[SerializeField]
	private int[] _unitHealth;


	/*
		Faction Stats Set-Up:
		------------------------------
		0 - OU/Swarm Attack
		1 - OU/Swarm Defense
		2 - DU Attack
		3 - DU Defense

		Unit Health Set-Up:
		------------------------------
		0 - OU
		1 - DU
		2 - Swarm
	*/


	// Private Variables:
	// n/a


	public bool Fight (UnitStructure attacker, UnitStructure defender) {
		int modifier;
		
		if (attacker.Type == UnitStructure.UnitStructureType.ATTACK)
			modifier = _ATTACK_MODIFIER;

		else
			modifier = _DEFENSE_MODIFIER;

		int damage = Mathf.Max (attacker.AttackPower - defender.DefensePower, modifier);
		GameManagerScript.Instance.UIManager.AppendTextToTerminal ("\n\nblitzsiege$  ./combat " + attacker.Owner.Name.ToUpper () + " " + defender.Owner.Name.ToUpper () + "\nCOMBAT SIM: Attack does " + damage + " damage to target.");
		defender.TakeDamage (damage);
		return defender.HasDied ();
	}



	// Returns the health assigned to the UnitStructure via _unitHealth
	public int GetHealthValue () {
		return _unitHealth[(int)GameManagerScript.Instance.Gameboard.CurrentUnitStructureAsset];
	}



	// Returns the offense power assigned to the UnitStructure of a given Faction
	public int GetOffenseValue () {
		switch (GameManagerScript.Instance.Gameboard.CurrentUnitStructureAsset) {
			case UnitStructure.UnitStructureType.TOWER:
			case UnitStructure.UnitStructureType.WALL:
			case UnitStructure.UnitStructureType.TRAP:
				return _structureOffenseStats[(int)GameManagerScript.Instance.Gameboard.CurrentUnitStructureAsset - 3]; // Structures start at index 3.

			case UnitStructure.UnitStructureType.ATTACK:
			case UnitStructure.UnitStructureType.SWARM:
				return _OUSwarmOffense[(int)GameManagerScript.Instance.CurrentPlayer.Faction - 1];
			
			case UnitStructure.UnitStructureType.DEFENSE:
				return _DUOffense[(int)GameManagerScript.Instance.CurrentPlayer.Faction - 1];
			
			default:
				Debug.Log ("Fatal errror! Can't find proper assignment for OFFENSE");
				return 1;	
		}
	}



	// Returns the defense power assigned to the UnitStructure of a given Faction
	public int GetDefenseValue () {
		switch (GameManagerScript.Instance.Gameboard.CurrentUnitStructureAsset) {
			case UnitStructure.UnitStructureType.TOWER:
			case UnitStructure.UnitStructureType.WALL:
			case UnitStructure.UnitStructureType.TRAP:
				return _structureDefenseStats[(int)GameManagerScript.Instance.Gameboard.CurrentUnitStructureAsset - 3]; // Structures start at index 3.

			case UnitStructure.UnitStructureType.ATTACK:
			case UnitStructure.UnitStructureType.SWARM:
				return _OUSwarmDefense[(int)GameManagerScript.Instance.CurrentPlayer.Faction - 1];
			
			case UnitStructure.UnitStructureType.DEFENSE:
				return _DUDefense[(int)GameManagerScript.Instance.CurrentPlayer.Faction - 1];
			
			default:
				Debug.Log ("Fatal errror! Can't find proper assignment for DEFENSE");
				return 1;	
		}
	}
}
