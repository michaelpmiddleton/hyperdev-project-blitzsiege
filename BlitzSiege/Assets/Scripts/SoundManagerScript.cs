﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundManagerScript : MonoBehaviour {
	[SerializeField]
	AudioSource _musicAudioSource;
	[SerializeField]
	AudioSource _sfxAudioSource;
	[SerializeField]
	AudioClip[] _tracks;
	[SerializeField]
	AudioClip[] _sfx;

	public void PlaySound (string soundID) {
		StopSound ();
		bool loaded = false;

		switch (soundID) {
			case "MenuNav":
				_sfxAudioSource.clip = _sfx[0];
				loaded = true;
				break;

			case "MenuSelect":
				_sfxAudioSource.clip = _sfx[1];
				loaded = true;
				break;
			
			case "Typing":
				_sfxAudioSource.clip = _sfx[2];
				loaded = true;
				break;

			case "Hit":
				_sfxAudioSource.clip = _sfx[3];
				loaded = true;
				break;

			case "Death":
				_sfxAudioSource.clip = _sfx[4];
				loaded = true;
				break;

			case "Explosion":
				_sfxAudioSource.clip = _sfx[5];
				loaded = true;
				break;

			case "Gameover":
				_sfxAudioSource.clip = _sfx[6];
				loaded = true;
				break;
		}

		if (loaded) {
			_sfxAudioSource.Play ();
		}
	}
	public void StopSound () {
		_sfxAudioSource.Stop ();
	}


	public void PlayTrack (string trackID) {
		StopTrack ();
		bool loaded = false;

		switch (trackID) {
			case "Main Menu":
				_musicAudioSource.clip = _tracks[0];
				loaded = true;
				break;

			case "Gameboard":
				_musicAudioSource.clip = _tracks[1];
				loaded = true;
				break;
		}

		if (loaded) {
			_musicAudioSource.Play ();
		}
	}
	public void StopTrack () {
		_musicAudioSource.Stop ();
	}
}
