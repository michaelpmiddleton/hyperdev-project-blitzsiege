﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HealthBarScript : MonoBehaviour {
	[SerializeField]
	private Vector3 _healthBarDefaultPosition;
	[SerializeField]
	private UnitStructure _parent;
	[SerializeField]
	private TextMeshProUGUI _readout;
	
	public void Initialize () {
		_parent = GetComponentInParent<UnitStructure> ();
		transform.localPosition = _healthBarDefaultPosition;
		name = "Health Output";
		UpdateOutput ();
	}

	public void UpdateOutput () {
		_readout.SetText (_parent.CurrentHealth.ToString ());
	}


	public void UpdateOrderInSortingLayer (int newOrder) {
		// _spriteRenderer.sortingOrder = newOrder;
	}
}
